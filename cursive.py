import curses
from curses import wrapper


def main(stdscr):
    stdscr.clear()
    stdscr.refresh()
    curses.curs_set(False)
    sample_data = [
        'First line',
        'Second line'
    ]
    ch = None
    while ch != ord('q'):
        stdscr.clear()
        row = 0
        for line in sample_data:
            stdscr.addstr(row, 0, line)
            row += 1
        stdscr.refresh()
        ch = stdscr.getch()


if __name__ == '__main__':
    wrapper(main)
